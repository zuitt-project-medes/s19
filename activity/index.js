let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"],
}

const {name, birthday, age, isEnrolled, classes} = student1

let studentA1 = `Hi! I'm ${name}. I am ${age} years old.`;
	
	console.log(studentA1);	

let studentA2 = `I study the following courses ${classes}`;
	console.log(studentA2);





function getCube(num){
	console.log(Math.pow(num, 3));
};

let cube = getCube(3);

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
});

let numSquared = numArr.map(function(num){
	return num ** 2;
});
console.log(numSquared);






class Dog {
    constructor(name, breed, age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }
};

let dog1 = new Dog("Frankie", "Miniature Dachshund",  3*7);
console.log(dog1);

let dog2 =  new Dog("Popoyes", "Yorkshire",  5*7);
console.log(dog2);